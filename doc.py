import sublime

from Anaconda.commands.doc import AnacondaDoc
from Anaconda.anaconda_lib.helpers import get_settings


class AnacondaFileDoc(AnacondaDoc):
    """Jedi get documentation string for Sublime Text and output into a file
    """
    def name(self):
        return 'anaconda_file_doc'

    def print_doc(self, edit):
        """Print the documentation string into a file
        """
        try:
            filename, mode = get_settings(self.view, 'anaconda_doc_output')

            with open(filename, mode) as f:
                f.write(self.documentation)
                f.write('\n\n')

        except ValueError:
            print('anaconda_doc_output expected: ["/path/to/file", "mode"]')

        except Exception as error:
            print('anaconda_doc:', error)

        else:
            self.documentation = None
            return  # no error, don't show panel

        super().print_doc(edit)
